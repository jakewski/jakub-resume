/*  DATABASE CREATION AND POPULATION
    This javascript file creates a local SQL database using sql.js, an open source project which compiled SQLite3 to javascript.
    I used this database for quick, lightweight data management for the purposes of my resume (which does not require a lot of data).
    It can also be repurposed for use with almost any SQL based database*/

/*----------------------------------------------------------------------------------------------------------------------------------*/

/*  Database Population: here is where the database data is stored. Data can be added to a table simply by adding a query to sqlstr. */
var db = new SQL.Database();


//Education table creation and data population
sqlstr = "CREATE TABLE education (year int, deg char, sem char, gpa float, mgpa float);";

sqlstr += "INSERT INTO education VALUES (2014, 'Bachelor of Science - Computer Science', 'Fall', 3.270, -1);INSERT INTO education VALUES (2015, 'Bachelor of Science - Computer Science', 'Spring', 3.500, 3.667), (2015, 'Bachelor of Science - Computer Science', 'Fall', 3.540, 3.8335), (2016, 'Bachelor of Science - Computer Science', 'Spring', 3.620, 4.000), (2016, 'Bachelor of Science - Computer Science', 'Fall', 3.600, 3.889);";


//courswork table creation and data population
sqlstr += "CREATE TABLE coursework (type char, name char, skills char);";

sqlstr += "INSERT INTO coursework VALUES ('math', 'Calculus', 'Attained an intermediate understanding of integration techniques, as well as applications in data analytics and statistics.'), ('compsci', 'Object Oriented Programming with C++', 'Gained knowledge of the C++ language and became comfortable with Object Oriented Programming practices.'), ('compsci', 'Artificial Intelligence', 'Became familiar with A.I. ideologies and many techniques including the use of search and optimization, logic (propositional and first order), and game playing algorithms. Additionally, I learned some machine learning fundamentals and practices.'), ('compsci', 'Data Structures', 'Obtained comfort with the use of a variety of data structures in various algorithms and forged an understanding for analyzing space and time complexity.'), ('math', 'Discrete Math', 'Moderate understanding of discrete structures and their uses in computer architecture, programming, and analytics.'), ('other', 'Technical Writing', 'Tasked with writing a variety of technical documents including memos, proposals, and recommendation reports. Collaborated with a group of fellow Computer Science and Engineering majors to create an instruction manual on the assembly of a home made drone with custom parts.');";


//work experience table creation and data population
sqlstr += "CREATE TABLE workexp (company char, role char, duties char, dates char);";

sqlstr += "INSERT INTO workexp VALUES ('Palmer Place Restaurant', 'Food Service Worker', 'As a deli worker, I was largely responsible for handling takeout orders and managing cash register. During my time working Palmer Place I have learned to effectively communicate with customers and coworkers, as well as satisfying customer needs and managing complaints. Since I was one of the most experienced workers in the deli, I was often required to manage multiple work stations and train new workers. ', 'March 2013 - July 2016'), ('Stacks - Penn State Harrisburg', 'Student Worker', 'For the majority of my work at Penn State Harrisburg, I was liable for cleaning kitchen and properly sanitizing food contact areas in the main cafeteria. I always demonstrated my adaptability to change by working several different shifts as necessary, even those in which I had little to no experience.', 'March 2015 - December 2016');";


//activities table creation

sqlstr += "CREATE TABLE skills (name char, type char, description char);";

sqlstr += "INSERT INTO skills VALUES ('Computer Enthusiast', 'compskill', 'I love keeping up to date with latest and greatest computer hardware. I have custom built PC that I enjoy maintaining and enhancing using newer hardware or software tweaks.'), ('Experimentation with Web Design', 'compskill', 'Self taught and familiar with many web development practices and technologies. I created this website using Bootstrap 3, HTML, JavaScript, CSS, and sql.js (SQLite compiled to JavaScript) as a local database. The website has a dynamic design such that in order to update aspects of my resume, I simply have to query the database to add or remove values to the proper table.'), ('Software Knowledge', 'compskill', 'Comfortable with Microsoft Word, Excel, PowerPoint, NetBeans, Eclipse, and Visual Studio. Experienced using Windows and Lunix(Ubuntu) operating systems.'), ('Soft Skills', 'other', 'I have strong written and verbal skills, and I am experienced in workplace communication and costumer satisfaction.');";

//test activity database fill
/*
sqlstr += "INSERT INTO skills VALUES ('Computer Enthusiast', 'compskill', 'I love keeping up to date with latest and greatest computer hardware. I have custom built PC that I enjoy maintaining and enhancing using newer hardware or software tweaks.'), ('Experimentation with Web Design', 'compskill', 'Self taught and familiar with many web development practices and technologies. I created this website using Bootstrap 3, HTML, JavaScript, CSS, and sql.js (SQLite compiled to JavaScript) as a local database. The website has a dynamic design such that in order to update aspects of my resume, I simply have to query the database to add or remove values to the proper table.'), ('Software Knowledge', 'compskill', 'Comfortable with Microsoft Word, Excel, PowerPoint, NetBeans, Eclipse, and Visual Studio. Experienced using Windows and Lunix(Ubuntu) operating systems.'), ('Soft Skills', 'other', 'I have strong written and verbal skills, and I am experienced in workplace communication and costumer satisfaction.'), ('Computer Enthusiast', 'compskill', 'I love keeping up to date with latest and greatest computer hardware. I have custom built PC that I enjoy maintaining and enhancing using newer hardware or software tweaks.'), ('Experimentation with Web Design', 'compskill', 'Self taught and familiar with many web development practices and technologies. I created this website using Bootstrap 3, HTML, JavaScript, CSS, and sql.js (SQLite compiled to JavaScript) as a local database. The website has a dynamic design such that in order to update aspects of my resume, I simply have to query the database to add or remove values to the proper table.'), ('Software Knowledge', 'compskill', 'Comfortable with Microsoft Word, Excel, PowerPoint, NetBeans, Eclipse, and Visual Studio. Experienced using Windows and Lunix(Ubuntu) operating systems.'), ('Soft Skills', 'other', 'I have strong written and verbal skills, and I am experienced in workplace communication and costumer satisfaction.'), ('Computer Enthusiast', 'compskill', 'I love keeping up to date with latest and greatest computer hardware. I have custom built PC that I enjoy maintaining and enhancing using newer hardware or software tweaks.'), ('Experimentation with Web Design', 'compskill', 'Self taught and familiar with many web development practices and technologies. I created this website using Bootstrap 3, HTML, JavaScript, CSS, and sql.js (SQLite compiled to JavaScript) as a local database. The website has a dynamic design such that in order to update aspects of my resume, I simply have to query the database to add or remove values to the proper table.'), ('Software Knowledge', 'compskill', 'Comfortable with Microsoft Word, Excel, PowerPoint, NetBeans, Eclipse, and Visual Studio. Experienced using Windows and Lunix(Ubuntu) operating systems.'), ('Soft Skills', 'other', 'I have strong written and verbal skills, and I am experienced in workplace communication and costumer satisfaction.'), ('Computer Enthusiast', 'compskill', 'I love keeping up to date with latest and greatest computer hardware. I have custom built PC that I enjoy maintaining and enhancing using newer hardware or software tweaks.'), ('Experimentation with Web Design', 'compskill', 'Self taught and familiar with many web development practices and technologies. I created this website using Bootstrap 3, HTML, JavaScript, CSS, and sql.js (SQLite compiled to JavaScript) as a local database. The website has a dynamic design such that in order to update aspects of my resume, I simply have to query the database to add or remove values to the proper table.'), ('Software Knowledge', 'compskill', 'Comfortable with Microsoft Word, Excel, PowerPoint, NetBeans, Eclipse, and Visual Studio. Experienced using Windows and Lunix(Ubuntu) operating systems.'), ('Soft Skills', 'other', 'I have strong written and verbal skills, and I am experienced in workplace communication and costumer satisfaction.'), ('Computer Enthusiast', 'compskill', 'I love keeping up to date with latest and greatest computer hardware. I have custom built PC that I enjoy maintaining and enhancing using newer hardware or software tweaks.'), ('Experimentation with Web Design', 'compskill', 'Self taught and familiar with many web development practices and technologies. I created this website using Bootstrap 3, HTML, JavaScript, CSS, and sql.js (SQLite compiled to JavaScript) as a local database. The website has a dynamic design such that in order to update aspects of my resume, I simply have to query the database to add or remove values to the proper table.'), ('Software Knowledge', 'compskill', 'Comfortable with Microsoft Word, Excel, PowerPoint, NetBeans, Eclipse, and Visual Studio. Experienced using Windows and Lunix(Ubuntu) operating systems.'), ('Soft Skills', 'other', 'I have strong written and verbal skills, and I am experienced in workplace communication and costumer satisfaction.'), ('Computer Enthusiast', 'compskill', 'I love keeping up to date with latest and greatest computer hardware. I have custom built PC that I enjoy maintaining and enhancing using newer hardware or software tweaks.'), ('Experimentation with Web Design', 'compskill', 'Self taught and familiar with many web development practices and technologies. I created this website using Bootstrap 3, HTML, JavaScript, CSS, and sql.js (SQLite compiled to JavaScript) as a local database. The website has a dynamic design such that in order to update aspects of my resume, I simply have to query the database to add or remove values to the proper table.'), ('Software Knowledge', 'compskill', 'Comfortable with Microsoft Word, Excel, PowerPoint, NetBeans, Eclipse, and Visual Studio. Experienced using Windows and Lunix(Ubuntu) operating systems.'), ('Soft Skills', 'other', 'I have strong written and verbal skills, and I am experienced in workplace communication and costumer satisfaction.'), ('Computer Enthusiast', 'compskill', 'I love keeping up to date with latest and greatest computer hardware. I have custom built PC that I enjoy maintaining and enhancing using newer hardware or software tweaks.'), ('Experimentation with Web Design', 'compskill', 'Self taught and familiar with many web development practices and technologies. I created this website using Bootstrap 3, HTML, JavaScript, CSS, and sql.js (SQLite compiled to JavaScript) as a local database. The website has a dynamic design such that in order to update aspects of my resume, I simply have to query the database to add or remove values to the proper table.'), ('Software Knowledge', 'compskill', 'Comfortable with Microsoft Word, Excel, PowerPoint, NetBeans, Eclipse, and Visual Studio. Experienced using Windows and Lunix(Ubuntu) operating systems.'), ('Soft Skills', 'other', 'I have strong written and verbal skills, and I am experienced in workplace communication and costumer satisfaction.');";
*/
//run all of the queries previously added to sqlstr
db.run(sqlstr); 


/*  DATABASE FUNCTIONS. These are used to quickly query the database and dynamically create html elements based on the data */

/*-------------------------------------------------------------------------------------------------------------------------------------*/

//EDUCATION.HTML

//test function
function sort_degree(degree){
  return db.exec("SELECT year, sem, gpa, mgpa FROM education WHERE deg = " + degree);
}

//fills table with id "psutable"; updates table based on database entries for penn state in education.
function fillpsutable(){
  var edutable = document.getElementById("psutable");
  var edutableContent = document.createElement("tbody");
  var res = sort_degree("'Bachelor of Science - Computer Science'");

  for (var i = 0; i < res[0].values.length; i++){
    var row = document.createElement("tr");

    for(var j = 0; j < res[0].values[i].length; j++){
      var cell = document.createElement("td");
      if(res[0].values[i][j] == -1){
        var content = document.createTextNode("N/A");
      }
      else{
        var content = document.createTextNode(res[0].values[i][j]);
      }
      cell.appendChild(content);
      row.appendChild(cell);
    }

    edutableContent.appendChild(row);
  }

  edutable.appendChild(edutableContent);


  //add footer with gpa averages
  var edufooter = document.createElement("tr");
  var avg = 0;
  var mavg = 0;
  var total = document.createElement("td");
  var totaltext = document.createTextNode("Average:");
  total.style.color = "blue";
  total.appendChild(totaltext);
  var factorout = 0;
  for(var i = 0; i < res[0].values.length; i++){
    avg += res[0].values[i][2];
    if(res[0].values[i][3] != -1){
      mavg += res[0].values[i][3];
    }
    else{
      factorout++;
    }
  }

  avg = avg / res[0].values.length;
  mavg = mavg / (res[0].values.length - factorout);


  var gpa = document.createElement("td");
  var gpatext = document.createTextNode(avg);
  gpa.appendChild(gpatext);
  gpa.style.color = "blue";

  var mgpa = document.createElement("td");
  var mgpatext = document.createTextNode(mavg);
  mgpa.style.color = "blue";

  mgpa.appendChild(mgpatext);

  edufooter.appendChild(document.createElement("td"));
  edufooter.appendChild(total);
  edufooter.appendChild(gpa);
  edufooter.appendChild(mgpa);

  edutableContent.appendChild(edufooter);

}

//categorizes fills the nav pills in education.html with coursework from the database
function fillcoursework(){

  var res = db.exec("SELECT * FROM coursework");
  
  for(var i = 0; i < res[0].values.length; i++){
    var par = document.createElement("p");
    var partxt = document.createTextNode(res[0].values[i][2]);
    var course = document.createElement("h4");
    var coursetxt = document.createTextNode(res[0].values[i][1]);
    par.appendChild(partxt);
    course.appendChild(coursetxt);

    var tab = document.getElementById(res[0].values[i][0])

    tab.appendChild(course);
    tab.appendChild(par);
  }


}

function fillworkexp(){

   var res = db.exec("SELECT * FROM workexp");

   for(var i = 0; i < res[0].values.length; i++){
      var company = document.createElement("h2");
      var dates = document.createElement("h3");
      var comptext = document.createTextNode(res[0].values[i][0] + " - " + res[0].values[i][1]);
      var datestext = document.createTextNode(res[0].values[i][3]);
      var duties = document.createElement("p");
      var dutiestext = document.createTextNode(res[0].values[i][2]);



      duties.appendChild(dutiestext);
      company.appendChild(comptext);
      dates.appendChild(datestext);

      var container = document.getElementById("workcontainer");
      container.appendChild(company);
      container.appendChild(dates);
      container.appendChild(duties);
   }
}

function fillskills(){

  var res = db.exec("SELECT * FROM skills");

  for(var i = 0; i < res[0].values.length; i++){
    var skill = document.createElement("h3");
    var skilltext = document.createTextNode(res[0].values[i][0]);

    var desc = document.createElement("p")
    var desctext = document.createTextNode(res[0].values[i][2]);

    desc.appendChild(desctext);
    skill.appendChild(skilltext);

    document.getElementById("skillcontainer").appendChild(skill);
    document.getElementById("skillcontainer").appendChild(desc);

  }
}

