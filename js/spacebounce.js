

function init(){
     var canvas = document.getElementsByTagName('canvas')[0];
     var c = canvas.getContext('2d');

     var container = {x:50,y:25,width:300,height:450};
     var char = {
			x:150,
			y:350,
			r: 25,
			vx: 1,
			vy: 4,
			color: "#00fceb",
			mass: 50
			};
		 var charv = {
			x:150,
			y:200,
			r: 35,
			vx: 0,
			vy: 0,
			color: "#7a174a",
			mass: 300
			};


     function draw(){
         c.fillStyle = 'black';
         c.strokeStyle = 'black';
        // c.fillRect(container.x,container.y,container.width,container.height);
         c.clearRect(container.x,container.y,container.width,container.height);
         //c.strokeRect(container.x,container.y,container.width,container.height);


             c.fillStyle = char.color;
             c.beginPath();
             c.arc(char.x, char.y, char.r,0,2*Math.PI,false);
             c.fill();

             if((char.x + char.vx + char.r > container.x + container.width - 50) || (char.x - char.r + char.vx < container.x)){
                char.vx = -char.vx;
             }
             if((char.y + char.vy + char.r > container.y + container.height - 20) || (char.y - char.r + char.vy < container.y)){
                char.vy = -char.vy;
             }


			 //////////////////////////////////////////////////////////////////////////////
			 c.fillStyle = charv.color;
             c.beginPath();
             c.arc(charv.x, charv.y, charv.r,0,2*Math.PI,false);
             c.fill();

             if((charv.x + charv.vx + charv.r > container.x + container.width - 50) || (charv.x - charv.r + charv.vx < container.x)){
                charv.vx = -charv.vx;
             }
             if((charv.y + charv.vy + charv.r > container.y + container.height - 20) || (charv.y - charv.r + charv.vy < container.y)){
                charv.vy = -charv.vy;
             }

			 			 var dist = Math.sqrt(Math.pow((charv.x - char.x), 2) + Math.pow((charv.y - char.y), 2))
						 if(dist < char.r + charv.r){
							 var tmp = char.vx;
							 var tmpy = char.vy;
							 char.vx = ((char.mass-charv.mass)/(char.mass+charv.mass)) * tmp + ((2*charv.mass)/(char.mass+charv.mass)) * charv.vx;
							 char.vy = ((char.mass-charv.mass)/(char.mass+charv.mass)) * tmpy + ((2*charv.mass)/(char.mass+charv.mass)) * charv.vy;
							 charv.vx = ((charv.mass-char.mass)/(char.mass+charv.mass)) * charv.vx + ((2*char.mass)/(char.mass+charv.mass)) * tmp;
							 charv.vy = ((charv.mass-char.mass)/(char.mass+charv.mass)) * charv.vy + ((2*char.mass)/(char.mass+charv.mass)) * tmpy;
							 charv.color = "#"+((1<<24)*Math.random()|0).toString(16);
						 }



							charv.x += charv.vx;
             				charv.y += charv.vy;
             				char.x += char.vx;
             				char.y += char.vy;


						document.onkeydown = checkKey;

						function checkKey(e) {

								e = e || window.event;

								if (e.keyCode == '38' && char.vy > -5) {
										// up arrow
										char.vy -= 1;
								}
								else if (e.keyCode == '40' && char.vy < 5) {
										// down arrow
										char.vy += 1;
								}
								else if (e.keyCode == '37' && char.vx > -5) {
									 // left arrow
										char.vx -= 1;
								}
								else if (e.keyCode == '39' && char.vx < 5) {
									 // right arrow
									 char.vx += 1;
								}
							  else if(e.keyCode == '32'){
									if(char.vx < 0){
										char.vx += 1;
									}
									if(char.vx > 0){
										char.vx -= 1 ;
									}
									if(char.vy < 0){
										char.vy += 1;
									}
									if(char.vy > 0){
										char.vy -= 1 ;
									}
								}

						}



         requestAnimationFrame(draw);

     }




    requestAnimationFrame(draw);


}

//invoke function init once document is fully loaded
window.addEventListener('load',init,false);




